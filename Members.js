const members = [
    {
        id: 1,
        name: 'Jane Doe',
        email: 'jane@gmail.com',
        status: 'active'
    },
    {
        id: 2,
        name: 'Karen Doe',
        email: 'karen@gmail.com',
        status: 'active'
    }
];

module.exports = members;
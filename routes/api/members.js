const express = require('express');
const members = require('../../Members');
const uuid = require('uuid');

// Init router
const router = express.Router();

// Get all members
router.get('/', (req, res) => {
    res.json(members);
});

// Get single member
router.get('/:id', (req, res) => {
    const id = req.params.id;
    const found = members.some(member => member.id === parseInt(id));
    if (found) {
       return res.json(members.filter(member => member.id === parseInt(id)));
    }
    return res.status(400).json({ msg: `No member with the id of ${id}`});
});

// Create member
router.post('/', (req, res) => {
    const newMember = {
        id: uuid.v4(),
        name: req.body.name,
        email: req.body.email,
        status: 'active'
    };
    if (!newMember.name || !newMember.email) {
        return res.status(400).json({ msg: 'Please include a name and email' });
    }
    members.push(newMember);
    res.json(members);
});

// Update member
router.put('/:id', (req, res) => {
    const id = req.params.id;
    const found = members.some(member => member.id === parseInt(id));
    if (!found) {
        return res.status(400).json({ msg: `No member with the id of ${id}`});
    }
    const reqMember = req.body;
    members.forEach(member => {
        if (member.id === parseInt(id)) {
            member.name = reqMember.name ? reqMember.name : member.name;
            member.email = reqMember.email ? reqMember.email : member.email;
            res.json({ msg: 'Member was updated', member });
        }
    });
});

// Delete member
router.delete('/:id', (req, res) => {
    const id = req.params.id;
    const found = members.some(member => member.id === parseInt(id));
    if (!found) {
        return res.status(400).json({ msg: `No member with the id of ${id}`});
    }
    res.json({ msg: 'Member deleted', members: members.filter(member => member.id !== parseInt(id))});
});

module.exports = router;
# ExpressJS Crash Course

ExpressJS Crash Course. 

Reference: https://www.youtube.com/watch?v=L72fhGm1tfE

# Installation

`npm install`

# Run Server

`npm run dev`

# API Usage

## Get all members 

`axios.get('http://localhost:5000/api/members')`

## Create member

```json
axios.post('http://localhost:5000/api/members', {
    name: 'Jane Doe',
    email: 'jane@gmail.com'
})
```

## Update member

```json
axios.put('http://localhost:5000/api/members/1', {
    name: 'Jane Doe',
    email: 'jane@gmail.com'
})
```

## Delete member

`axios.delete('http://localhost:5000/api/members/1')`